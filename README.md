# Austrian NeuroCloud website

The website of the Austrian NeuroCloud project: https://ccns.gitlab.io/neurocog/neurodataops/anc/media/anc-website

## Design

The website is implemented using [Pelican](https://docs.getpelican.com/en/latest/index.html) static site generator with the Markdown extension allowing the content be written using Markdown.

The graphical design is powered by [Bulma](https://bulma.io/). At the moment we use the defaults with a [few modifications](https://gitlab.com/ccns/neurocog/neurodataops/anc/media/anc-website/-/blob/main/themes/anc/static/sass/anc.scss) (responsive CSS-only menu, accordion). The CSS is then compiled with [Sass](https://sass-lang.com/).

## Structure

`content` directory holds all the content. The pages that appear in the header menu are located in `content/pages`.

`themes/anc` holds the Pelican templates files and custom CSS.

## Deployment

We use GitLab CI/CD to build and publish the website with GitLab Pages. For development we use merge requests which cause slightly different pipeline.

To review the development version, push the changes to a merge request and access it through GitLab menu: Deployments/Environments.

## Building locally

Make sure you have Python installed. For Windows we recoomend to to use the newest [Installer Release](https://www.python.org/downloads/windows/).

**All commands are executed from the project root.**

There are three dependencies: Pelican, Sass, and Bulma.

1. [Install Pelican](https://docs.getpelican.com/en/latest/install.html). We recommend to create a virtual environment using the [`venv` module](https://docs.python.org/3/library/venv.html).
   - Linux
      ```
      python -m venv .venv
      source .venv/bin/activate
      python -m pip install -r requirements.txt
      ```
   - Windows
      ```
      python -m venv .venv
      .venv\Scripts\activate
      python -m pip install -r requirements.txt
      ```
2. [Download Sass](https://sass-lang.com/install) and put the executable in the project root.
   - Linux
      ```
      wget -O dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/1.69.5/dart-sass-1.69.5-linux-x64.tar.gz
      tar -xf dart-sass.tar.gz
      rm dart-sass.tar.gz
      ```
   - Windows
      ```
      curl.exe -L -o dart-sass.zip https://github.com/sass/dart-sass/releases/download/1.69.5/dart-sass-1.69.5-windows-x64.zip
      tar -xf dart-sass.zip
      del dart-sass.zip
      ```
3. [Download Bulma](https://bulma.io/documentation/customize/with-sass-cli/#2-download-bulma).
   - Linux
      ```
      wget https://github.com/jgthms/bulma/releases/download/0.9.4/bulma-0.9.4.zip
      unzip bulma-0.9.4.zip
      rm bulma-0.9.4.zip
      ```
   - Windows
      ```
      curl.exe -L -o bulma-0.9.4.zip https://github.com/jgthms/bulma/releases/download/0.9.4/bulma-0.9.4.zip
      tar -xf bulma-0.9.4.zip
      del bulma-0.9.4.zip
      ```
4. Compile the CSS.
   - Linux
      ```
      ./dart-sass/sass --no-source-map themes/anc/static/sass/anc.scss:themes/anc/static/css/anc.css
      ```
   - Windows
      ```
      .\dart-sass\sass --no-source-map themes\anc\static\sass\anc.scss:themes\anc\static\css\anc.css
      ```
5. Generate content (Linux and Windows).
   ```
   python -m pelican content/ -s pelicanconf.py -t themes/anc/
   ```
6. View the landing page `output/index.html` in your browser.

## License

The content of this website is the subject to [CC BY license](https://creativecommons.org/licenses/by/4.0/).
