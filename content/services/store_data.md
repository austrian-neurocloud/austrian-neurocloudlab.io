---
Title: Store data
date: 2023-04-19
order: 1
teaser: Bring your data to ANC and benefit from comprehensive curation and continuous data operations.
icon: cloud-upload-outline
link: https://handbook.anc.plus.ac.at/anc-basics/self_deposit_data.html
linktext: Self deposit guidelines
---