---
Title: FIRST OFFICIAL EUROPEAN NEUROBAGEL NODE
Date: 2024-12-03
Summary: Another bite in the bagel! We are delighted to announce that we are officially the first public european node in the [Neurobagel](https://neurobagel.org/) universe! You want to have a look at datasets yourself and contribute to our project? Check out out service section for further information!
Image1: neurobagel_node.png
Image2: anc_neurobagel_plus.png
---