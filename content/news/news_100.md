---
Title: Presentation of the ANC in a new paper
Date: 2024-04-01
Summary: [We published a new paper presenting the ANC](https://doi.org/10.21240/zfhe/SH-A/08) in an Austrian Journal on Higher Education Development. Our PI Florian Hutzler and scientific coordinator Nicole Himmelstoß present the ANC as a showcase repository for FAIR and TRUSTworthy research data management. 
Image2: florian_nicole1.png
---
