---
Title: ANC at SAMBA 2024
Date: 2024-07-11
Summary: Our PhD student Barbara presented a poster showcasing the ANC at [SAMBA 2024](https://samba.ccns.sbg.ac.at/samba-2024/). With a whole bunch of insightful exchanges with colleagues and potential future users of the ANC, this was a resounding success. We're encouraged by the positive response and excited about the opportunities ahead.
Image1: samba1.jpg
Image2: samba2.jpg
---