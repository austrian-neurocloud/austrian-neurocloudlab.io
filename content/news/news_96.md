---
Title: Visitors from Montreal - Neurobagel and Nipoppy
Date: 2024-07-10
Summary: We were delighted to host Sebastian Urchs and Nikhil Bhagwat from the exciting [Neurobagel project](https://neurobagel.org/) based in the ORIGAMI Lab at the Montreal Neurological Institute, Canada. We had productive discussions and mutually profited from one another's expertise and challenges experienced in trying to advance the management of neuro-cognitive research data. This collaboration promises to enhance our efforts and drive forward innovative solutions in our field.
Image1: neurobagel1.jpeg
Image2: neurobagel2.jpg
---