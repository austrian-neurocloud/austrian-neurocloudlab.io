---
Title: PSY TALKS - OPEN RESEARCH DATA
Date: 2024-10-02
Summary: Our Head of Project, Univ.Prof. Dr. Florian Hutzler, recently gave a speech on the possibilities of open research data. By promoting the principles of open science, we aim to expand the use of open research and encourage more people to contribute to our project.
Image1: florian_poster.png
Image2: florian_presentation.png
---