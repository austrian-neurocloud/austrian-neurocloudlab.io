---
Title: New paper out on the past, present, and future of BIDS!
Date: 2024-05-01
Summary: Our Senior Scientist Mateusz Pawlik contributed to a [recently published article](https://doi.org/10.1162/imag_a_00103) outlining the history and future vision of BIDS, the community standard for organizing brain imaging data.
Image1: mateusz_bids1.png
Image2: mateusz_bids2.png
---
