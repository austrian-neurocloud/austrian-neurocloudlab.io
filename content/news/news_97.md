---
Title: Anna's defensio
Date: 2024-05-21
Summary: "Is there a doctor on board?" Indeed, there is!!! Is it about data? Do we have a severe case of ontological constructivism? Either way, Anna can help for sure. Congratulations to Dr. Anna Ravenschlag for successfully defending her PhD dissertation titled 'It's about data - How ontology-based semantic enrichment of neurocognitive data can aid knowledge valorization and FAIR data operations in Cognitive Neuroscience'
Image1: anna_defensio1.png
Image2: anna_defensio2.png
---