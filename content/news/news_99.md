---
Title: PsychoPy/JS code sprint
Date: 2024-05-15
Summary: Our PhD student Monique Denissen participated in the [2024 PsychoPy/JS code sprint at the University of Nottingham](https://discourse.psychopy.org/t/2024-psychopy-js-code-sprint-15-17-april/38150) from 15-17 April 2024. She was invited to work on the [BIDS plug-in](https://psychopy-bids.readthedocs.io/en/latest/) and talk about data standards. A more automatic generation of BIDS events will allow users without much coding experience to adhere to BIDS standards using the PsychoPy Builder only.  
Image1: psychopy_codesprint1.png
---
