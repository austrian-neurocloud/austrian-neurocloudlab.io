---
Title: Attending the Cluster Forschungsdaten EXPO 2025
Date: 2025-01-28
Summary: On January 16th, the ANC had the amazing opportunity to attend the Cluster Forschungdaten EXPO! It was an inspiring experience filled with insightful talks, great networking and exiting ideas for the future.
Image1: cluster_forschungsdaten_1.jpeg
Image2: cluster_forschungsdaten_2.jpeg
---