---
Title: Maksim Dounar-Zapolski
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Maksim
Surname: Dounar-Zapolski
Order: 43
Position: System Administrator
Email: maksim.dounar-zapolski@plus.ac.at
Image: male.jpeg
---
