---
Title: Stefan Hawelka
Date: 2022-06-08
Name: Stefan
Surname: Hawelka
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Order: 26
Position: Associated researcher
Email: stefan.hawelka@plus.ac.at     
Image: male.jpeg
---
