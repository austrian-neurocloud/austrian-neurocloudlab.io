---
Title: Karl Koschutnig
Date: 2022-06-08
Name: Karl
Surname: Koschutnig
Affiliations:
    University of Graz
    Faculty of Natural Sciences
    Department of Psychology
Order: 23
Position: Scientific Consultant
Email: karl.koschutnig@uni-graz.at
Image: koschutnig.jpeg
---
