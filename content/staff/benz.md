---
Title: Kaja Benz
Date: 2022-06-08
Name: Kaja
Surname: Benz
Affiliations:
    University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Order: 37
Position: Data Steward - MEG Data
Email: kajarosa.benz@plus.ac.at
Image: benz.jpeg
---
