---
Title: Matthias Schurz
Date: 2022-06-08
Name: Matthias
Surname: Schurz
Affiliations:
    University of Innsbruck
    Institute of Psychology
Order: 25
Position: Associated researcher
Email: Matthias.Schurz@uibk.ac.at
Image: schurz.jpeg
---
