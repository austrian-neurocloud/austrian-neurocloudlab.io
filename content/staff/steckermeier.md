---
Title: Eva Steckermeier
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Eva
Surname: Steckermeier
Order: 34
Position: Data Steward - MRI Data
Email: eva.steckermeier@plus.ac.at
Image: steckermeier.jpg
---
