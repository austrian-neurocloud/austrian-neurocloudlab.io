---
Title: Florian Hutzler
Date: 2022-06-08
Name: Florian
Surname: Hutzler
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Order: 11
Position: Head of project
Email: florian.hutzler@plus.ac.at
Phone:  +4366280445114
Image: hutzler.jpeg
---
