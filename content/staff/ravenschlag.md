---
Title: Anna Ravenschlag
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Anna
Surname: Ravenschlag
Order: 33
Position: Knowledge Engineer & Open Science Consultant
Email: annanatali.ravenschlag@plus.ac.at
Image: ravenschlag.jpeg
---
