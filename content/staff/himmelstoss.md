---
Title: Nicole Himmelstoß
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Nicole
Surname: Himmelstoß
Order: 22
Position: Scientific Coordinator
Email: nicolealexandra.himmelstoss@plus.ac.at
Image: female.jpeg
---
