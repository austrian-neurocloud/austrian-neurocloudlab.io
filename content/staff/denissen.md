---
Title: Monique Denissen
Date: 2022-06-08
Name: Monique
Surname: Denissen
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Order: 31
Position: Data Steward - Task Experiment Data
Email: monique.denissen@plus.ac.at
Image: denissen.jpeg
---
