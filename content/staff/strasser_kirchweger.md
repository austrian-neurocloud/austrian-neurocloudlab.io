---
Title: Barbara Strasser-Kirchweger
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Barbara
Surname: Strasser-Kirchweger
Order: 35
Position: Data Steward - Questionnaire Data
Email: barbara.strasser-kirchweger@plus.ac.at
Image: strasser_kirchweger.jpg
---
