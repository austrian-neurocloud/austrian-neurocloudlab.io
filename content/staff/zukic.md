---
Title: Edvin Zukic
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Law, Business and Economics
    Department of Public Law
Name: Edvin
Surname: Zukic
Order: 48
Position: Legal Consultant
Email: edvin.zukic@plus.ac.at
Image: male.jpeg
---
