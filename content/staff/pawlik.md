---
Title: Mateusz Pawlik
date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Mateusz
Surname: Pawlik
Order: 21
Position: Lead Developer
Email: mateusz.pawlik@plus.ac.at
Phone:  +43 662 804451-70
Image: pawlik.jpeg
---
