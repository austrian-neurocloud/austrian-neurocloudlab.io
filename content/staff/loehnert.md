---
Title: Bianca Löhnert
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Bianca
Surname: Löhnert
Order: 32
Position: Developer - Querying Backend
Email: bianca.loehnert@plus.ac.at
Image: loehnert.jpg
---
