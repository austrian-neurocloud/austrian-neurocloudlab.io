---
Title: Thomas Konrad
Date: 2022-06-08
Name: Thomas
Surname: Konrad
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Order: 49
Position: Helping Hand & PR
Email: thomastill.konrad@plus.ac.at
Image: konrad.jpg
---
