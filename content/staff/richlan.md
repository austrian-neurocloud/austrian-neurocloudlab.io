---
Title: Fabio Richlan
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Fabio
Surname: Richlan
Order: 24
Position: Associated researcher
Email: fabio.richlan@plus.ac.at
Image: richlan.jpeg
---
