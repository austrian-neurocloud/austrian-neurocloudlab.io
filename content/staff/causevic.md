---
Title: Amir Causevic
Date: 2022-06-08
Name: Amir
Surname: Causevic
Affiliations:
    University of Graz
    Faculty of Natural Sciences
    Department of Psychology
Order: 41
Position: Associated researcher
Email: amir.causevic@uni-graz.at
Image: causevic.jpeg
---
