---
Title: Andreas Huttegger
Date: 2022-06-08
Affiliations:
    Paris Lodron University of Salzburg
    Faculty of Natural and Life Sciences
    Department of Psychology
Name: Andreas
Surname: Huttegger
Order: 42
Position: System Administrator
Email: andreas.huttegger@plus.ac.at
Image: male.jpeg
---
