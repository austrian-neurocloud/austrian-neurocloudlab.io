---
Title: Anja Ischebeck
Date: 2022-06-08
Name: Anja
Surname: Ischebeck
Affiliations:
    University of Graz
    Faculty of Natural Sciences
    Department of Psychology
Order: 12
Position: Associated researcher
Email: anja.ischebeck@uni-graz.at
Image: ischebeck.jpg
---
