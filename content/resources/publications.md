---
Title: Publications
Date: 2024-01-17
order: 3
teaser: Find out about our publications and external links.
icon: book-outline
---

### Citing the Austrian NeuroCloud

When referring to the Austrian NeuroCloud repository in a publication, please cite the following article:

Hutzler, F., & Himmelstoß, N. (2024). Austrian NeuroCloud: FAIRes und vertrauenswürdiges Forschungsdatenmanagement. *Zeitschrift für Hochschulentwicklung, 19 (Sonderheft Administration)*, 117–142. [https://doi.org/10.21240/zfhe/SH-A/08](https://doi.org/10.21240/zfhe/SH-A/08)

### All publications related to the ANC (arranged by work packages)

#### Research Data Lifecycle
Hutzler, F., & Himmelstoß, N. (2024). Austrian NeuroCloud: FAIRes und vertrauenswürdiges Forschungsdatenmanagement. *Zeitschrift für Hochschulentwicklung, 19 (Sonderheft Administration)*, 117–142. [https://doi.org/10.21240/zfhe/SH-A/08](https://doi.org/10.21240/zfhe/SH-A/08) [*Funded by [ANC]({filename}../funders_partners/bmbwf.md) & [DNI]({filename}../funders_partners/land.md)*]


#### Classification of Data
Denissen, M., Richlan, F., Birklbauer, J., Pawlik, M., Ravenschlag, A. N., Himmelstoss, N., Hutzler, F.,Robbins, K. (2023, January 16). Actionable event annotation and analysis in fMRI: A practical guide to event handling. [https://doi.org/10.31219/osf.io/xdbrv](https://doi.org/10.31219/osf.io/xdbrv) [*Funded by [ANC]({filename}../funders_partners/bmbwf.md)*]

Ravenschlag, A., Löhnert, B., Guizzardi, G., da Silva Teixeira M. d. G., Denissen, M., and Hutzler, F.
CoTOn: A Cognitive Theory Ontology for representing diverging conceptualizations of cognitive concepts.*Proceedings of the Ontology showcase at Formal Ontology in Information Systems Conference (July 17–20, 2023, Quebec, Canada). CEUR Workshop*. [https://doi.org/10.25598/ceur-ws-3637-45](https://doi.org/10.25598/ceur-ws-3637-45) [*Funded by [ANC]({filename}../funders_partners/bmbwf.md) & [DNI]({filename}../funders_partners/land.md)*]

Ravenschlag, A., Denissen, M., Löhnert, B., Pawlik, M., Himmelstoß, N., & Hutzler, F. (2023). Effective queries for mega-analysis in cognitive neuroscience. *CEUR Workshop Proceedings, 3379*. [https://ceur-ws.org/Vol-3379/CoMoNoS_2023_id252_Mateusz_Pawlik.pdf](https://ceur-ws.org/Vol-3379/CoMoNoS_2023_id252_Mateusz_Pawlik.pdf) [*Funded by [ANC]({filename}../funders_partners/bmbwf.md) & [DNI]({filename}../funders_partners/land.md)*]


#### Data privacy, security, and autonomy

Sebastian Krempelmeier's talk "Privilegierung bei Verarbeitung aufgrund von § 2d Abs 2 Z1 iVm Abs 1 FOG" at the administrative law department's institutional conference - published conference proceedings:
Jahnel, D., Krempelmeier, S., & Schmid, S. (2024). *Wissenschaft und Datenschutz: Sechste Jahrestagung Räume und Identitäten des Fachbereichs Öffentliches Recht. (Räume und Identitäten)*. Jan Sramek Verlag. [*Funded by [DNI]({filename}../funders_partners/land.md)*]


#### Data management

Poldrack, R. A., Markiewicz, C. J., Appelhoff, S., Ashar, Y. K., Auer, T., Baillet, S., Bansal, S., Beltrachini, L., Benar, C. G., Bertazzoli, G., Bhogawar, S., Blair, R. W., Bortoletto, M., Boudreau, M., Brooks, T. L., Calhoun, V. D., Castelli, F. M., Clement, P., Cohen, A. L., ... Gorgolewski, K. J. (2024). The Past, Present, and Future of the Brain Imaging Data Structure (BIDS). *Imaging Neuroscience, 2*, 1-19. [https://doi.org/10.1162/imag_a_00103](https://doi.org/10.1162/imag_a_00103)


### External links

* [Digital University Hub](https://www.digitaluniversityhub.eu/dx-initiativen/alle-initiativen/in-forschung/austrian-neurocloud?sword_list%5B0%5D=austrian&sword_list%5B1%5D=neurocloud&no_cache=1)
* [Forschungsdaten.info](https://forschungsdaten.info/fdm-im-deutschsprachigen-raum/oesterreich/projekte/austrian-neurocloud/)
* [University of Graz](https://cognitive-psychology.uni-graz.at/en/projects/austrian-neurocloud/)
* [PLUS Research](https://uni-salzburg.elsevierpure.com/de/projects/austrian-neuro-cloud)
* [Re3Data](https://www.re3data.org/repository/r3d100014355)
* [University of Innsbruck](https://www.uibk.ac.at/de/universitaet/digitalisierung/digitalisierungsprojekte/)

### Media coverage

* [Die Presse, 2023](https://www.diepresse.com/6278721/das-gehirn-oeffnen-nicht-chirurgisch-sondern-auf-datenbasis)
* [Salzburger Nachrichten, 2020](https://www.plus.ac.at/wp-content/uploads/2021/02/UN-12-16-1.pdf)
* [Salzburg24, 2020](https://www.salzburg24.at/news/salzburg/digitalisierungsoffensive-uni-salzburg-erhaelt-zuschlag-fuer-zwei-projekte-82278397)
* [Austria Presse Agentur, 2021](https://science.apa.at/power-search/4191403358886532819)
* [Kronen Zeitung, 2020](https://www.krone.at/2081907)
* [My Science, 2020](https://www.myscience.at/news/wire/digitalisierungsoffensive_des_bundes_uni_salzburg_punktet_mit_innovativen_projekten-2020-uni-salzburg)
* [Trending Topics, 2020](https://www.trendingtopics.eu/50-millionen-euro-oesterreichische-universitaeten/)