---
Title: Why
Date: 2024-01-17
section: 2
icon: help-outline
buttontext: Learn about our drivers
subtitle: The ANC provides the basis for sustainable neurocognitive research data management.
teaser: The need for a domain-specific repository to store and share neurocognitive research data arises from a set of circumstances that lie in past, present, and future. 
---

**Past**

Empirical research has historically followed a simple scheme for using the data gathered for scientific purposes: One (team of) researcher(s) gathers data, analyzes and publishes it, and then stores it (first analogue, later digitally) at the affiliated institution. Though occasionally the same researcher(s) reused it for a different investigation, **data was commonly used only once**. For a variety of reasons (e.g., constantly increasing ranges of methods, intensifying demands for open science practices etc.), producing and storing data in a sustainable way was becoming increasingly popular. This was intended allow for increased research transparency and data reusability beyond the original purpose (and institution) collected for.

**Present**

In an attempt to accommodate the growing demands for a more sustainable data management, parallel efforts have emerged to create a variety of open tools and services to help with research data production and storage. Despite providing pioneering ground work, many of the current efforts do not integrate all necessary means for sustainable data management. Importantly, Austrian funding agencies abide by the _Open Science Policy Austria_, which requires data originating from publicly funded research to be **FAIR** and stored – if possible – in a **domain-specific**, **certified**, and **trusted data repository**. Accommodating and appreciating these national developments towards more open science, the ANC aspires to mitigate mere "data dumping” and redirect towards sustainable research data management across Austria. 

**Future**

With the the _European Open Science Cloud_ (EOSC), a federated research infrastructure (i.e., a _system of systems_) has been initiated on the European level. As a national, domain-specific, and FAIR-enabling research data infrastructure, the ANC strives and prepares for an **EOSC membership** in the near future.

