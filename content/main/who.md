---
Title: Team
Date: 2024-01-17
section: 4
icon: people-outline
buttontext: Get to know us
subtitle: The ANC is run by a muti-professional team of computer scientists, psychologists, and cognitive neuroscientists.
teaser: The ANC received state-level funding (2020-2024) and is currently striving to move beyond project-status. At the University of Salzburg, we aspire to secure the necessary resources to maintain and evolve the ANC as a sustainable information system.
template: team
---

We are an interdisciplinary team of psychologists, cognitive neuroscientists and computer scientists, which builds upon the scientific expertise and strength of both established and aspiring young scientists. Lead by the [University of Salzburg](https://www.plus.ac.at/?lang=en), our project includes partners from the [Centre for Cognitive Neurosciences Salzburg (CCNS)](https://ccns.plus.ac.at/), the [Paracelsus Medical University of Salzburg](https://www.pmu.ac.at/en/home.html), the [Christian-Doppler-Klinik](https://salk.at/Christian-Doppler-Klinik.html), the [University of Graz](https://www.uni-graz.at/en/), the [University of Innsbruck](https://www.uibk.ac.at/index.html.en), and the [University of Vienna](https://www.univie.ac.at/en/).
