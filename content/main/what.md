---
Title: What
Date: 2024-01-17
section: 1
icon: cloud-outline
buttontext: Read about the project
subtitle: The ANC is a FAIR-enabling open repository for neurocognitive research data.
teaser: With the ANC, we bring forth a place to securely store neurocognitive research data and provide innovative tools and services for data maintenance and utilization.
---

The ANC is a FAIR-enabling open repository providing tools and services for the storage, maintenance, and utilization (e.g., through (re)-analysis or integration) of neurocognitive research data. The ANC fully supports the mission of the _European Open Science Cloud_ (EOSC) and is commited to the European Union's open science policy, legal standards, and best open science practices. We aspire to provide the framework necessary for researchers to facilitate data operations along the entire research data lifecycle. This includes **planning and designing** research studies (with an increasing amount of funding agencies requiring a stringent _data management plan_),  **organizing and preparing** data (by adhering to accepted _community standards_), **processing and analyzing** data (using a system of _integrated tools_), and finally **storing and sharing** the research data (promoting the ongoing shift in research culture towards heightened _transparency_, _data reusability_, and _result reproducibility_).
