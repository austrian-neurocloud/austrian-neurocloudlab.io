---
Title: How
Date: 2024-01-17
section: 3
icon: accessibility-outline
buttontext: Discover how we operate
subtitle: The ANC implements and endorses best practices within the neurocognitive research community.
teaser: As a fully integrated environment for data storage, administration, and analysis, the ANC is more than up to the job for modern sunstainable data management. Using the ANC means using community-wide accepted standards for data annotation, state-of-the-art analysis rountines, and upt-to-date maintenance and protection tools.
---

**Organization and Preprocessing**

*Data organization.*
To ensure full _syntactic data interoperability_, the ANC requires datasets to adhere to the _Brain Imaging Data Structure_ (BIDS; Gorgolewski et al., 2016). BIDS specifies a standardized structure for storing data in a hierachical file system. When uploading a dataset to the ANC, a BIDS validator automatically checks for compliance with the current BIDS version. In accordance with the FAIR principles, standardized data organization greatly facilitates the _accessibility_ and _syntactic interoperability_ of the data stored on the ANC.

*Data classification.*
The ANC commits to machine-readable and machine-actionable _metadata_ via _Hierarchical Event Descriptors_ (HED, Robbins et al., 2021) and a domain-specific schema extension (Denissen et al., in press). Complementing the syntactic interoperability ensured by BIDS, standardized metadata annotations enable _semantic data interoperability_, i.e. data can be assessed and integrated based on meaningful context descriptions. Specifically, HED annotations allow to exhaustively describe the experimental setting, i.e. _how_ the data was collected. Providing data with meaningful metadata is peculiar relevance for FAIR data sharing because it significantly improves the data's _findability_ and _reusability_. 

**Processing and Analysis**

As a fully intergrated environment for sustainable research data management, the ANC also provides _standard analysis rountines_ for neuroimaging data. The handling of fMRI data, for instance, is facilitated through state-of-the-art open source preprocessing (_fMRIprep_) and data analysis software (_FitLins_). Through containerization, analysis workflows are reproducible on any hardware and make full use of HPC systems.

**Archiving and Reuse**

*Object maintenance.*
Digital objects stored on the ANC are subject to stringent quality control (ensuring integrity of both research data and metadata), version control, and control of data lineage. 

*Data protecton and licensing.*
Data handling in the ANC is in accordance with the _General Data Protection Regulation_ (GDPR) of the Eurpean Union. In the context of the _Digital Neuroscience Initiative_ (2021), a team of legal experts and scientists carefully considers the intricate interplay between academic freedom and the protection of privacy. This ensures a continuously sound legal basis for datasets stored on the ANC. Endorsing open and non-discriminatory data sharing, as far as legally possible and desired by the data owners, the ANC preferredly awards Creative Commons licenses.
