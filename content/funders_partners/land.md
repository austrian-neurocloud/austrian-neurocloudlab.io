---
Title: Funding by the Land Salzburg
date: 2023-04-19
order: 2 
website: https://www.salzburg.gv.at/
Image: logo_land.svg
related_project1: Digital Neuroscience Initiative (DNI, 2021-24)
related_project2: AI-based identification of "long COVID"-related diagnostic pathways (AIDA-PATH, 2021-25)
---

The [Land Salzburg](https://www.salzburg.gv.at/) has (co-)funded two projects that are deemed to contribute substantially to the relization of the ANC by expanding it both on a conceptual and technical level of implementation to the more clinical side of neurocognitive research data: the [Digital Neuroscience Initiative (DNI)](#digital-neuroscience-initiative) and the [AI-based Identification of 'long COVID'-related diagnostic pathways](#ai-based-identification-of-long-covid-related-diagnostic-pathways)

## <u>Digital Neuroscience Initiative</u>
#### Funding period: 01.07.21 - 31.09.24
#### Funding sum: EURO 565.744,-

In a collaborative effort of the Department of Psychology & Centre for Cognitive Neuroscience ([Univ.-Prof. Dr. Florian Hutzler](https://ccns.plus.ac.at/labs/neurocog/members/florian_hutzler/)), the Department of Public Law ([Univ.-Prof. Dr. Sebastian Schmid](https://www.plus.ac.at/oeffentliches-recht/fachbereich/verfassungs-und-verwaltungsrecht/mitarbeiterinnen-vvr/schmid-sebastian/)) and the Department of Computer Science ([Univ.-Prof. Dr. Nikolaus Augsten](https://dbresearch.uni-salzburg.at/~augsten)), the Digital Neuroscience Initiative (DNI) plays a fundamental role in realizing our vision of the Austrian NeuroCloud. 

In short, this project aims to (1) clarify the legal basis for the digitalization of human data, (2) develop innovative tools for the efficient management of Big Human Neurodata, and (3) to expand the ANC into a pan-European Open Neuroscience platform. 

###### (1) Project management and controlling
The first important work package of this project concerns setting up management tools and a communication platform, organizing meetings, and workshops for researchers and local businesses, and managing communication and reporting. It ensures progress and goal achievement through regular evaluations, risk management, and compliance checks.

###### (2) Data protection principles and Big Human Data
We seek to establish a legal basis for the handling of Big Human Data under the General Data Protection Regulation (GDPR) and the Austrian Forschungsorganisationsgesetz (FOG). The goal is to clarify the data protection status of brain data, which are considered health data under GDPR and have strict processing conditions unless specific exceptions apply. The project will analyze how the [FAIR principles](https://www.go-fair.org/fair-principles/) align with GDPR and FOG to evaluate the legal basis from an Open Science perspective. It will also assess data anonymization, determining the effort needed to re-identify neurocognitive raw data and which data can be provided under FOG.

###### (3) Modeling a Cognitive Ontology and linking it to Big Human Neurodata
 Developing and implementing a new Cognitive Ontology in a formal, machine-readable language is a main pillar of the DNI. It should subsequently be used to search Big Human Neurodata. Additionally, innovative techniques for syntactic and semantic similarity search are developed to align heterogeneous metadata with well-defined concepts.

###### (4) Dissemination - knowledge transfer ANC-PLUS & Strategic Networking
We develop a Data Management Plan (FDM) and Open Data Policy to guide the entire data lifecycle, ensuring alignment with FAIR principles. Additionally, we create service points to support researchers, students, and young scientists with FDM and Open Science practices. 

###### (5) Austrian XSite Open Node (AXON) -High-Performance Computing
We assessed the storage and processing needs based on the average number of subjects from the past two years at various locations, including Salzburg, Innsbruck, Graz, and Vienna. We identified the optimal installation site for high-density racks and storage, started the procurement process, and plan to oversee the installation. 

## <u>AI-based Identification of 'long COVID'-related diagnostic pathways</u>
#### Funding period: 01.11.2021 - 31.10.2024
#### Funding sum: EURO 464.004,-

The project 'AI-based indentification of "long COVID"-related diagnostic pathways' (AIDA-PATH) is a collaboration between the German software company [medicalvalues](https://medicalvalues.de/de/), the [IDA Lab at PLUS and PMU](https://www.plus.ac.at/aihi/der-fachbereich/ida-lab/) and the [Centre for Cogntitive Neuroscience](https://ccns.plus.ac.at/) at Paris-Lodron University Salzburg. It seeks to develop AI-based software for efficient and methodologically valid diagnosis and tracking of conditions like COVID-19, particularly focusing on psychological and cognitive parameters. The project plays a key role in expanding the ANC by integrating a new ontology necessary for the analysis, operationalization, and representation of psychological parameters.

Work packages 1-4 and 7-8 are handled by our collaborator [Wolfgang Trutschnig](https://www.plus.ac.at/aihi/der-fachbereich/team/dr-wolfgang-trutschnig/?lang=en) from the Department of Artificial Intelligence and Human Interfaces. 

###### (5) COVID-19 Fatigue: Psychological Parameters
This work package involves analyzing current research on COVID-19 Fatigue to identify psychological parameters (e.g., fatigue, apathy, depressive symptoms, reduced cognitive performance) linked to the condition. In this context, we also review state-of-the-art methods for operationalizing these parameters. Existing ontologies for mental disorders are being assessed to see if they can represent COVID-19 Fatigue parameters and if they can be made interoperable with other relevant ontologies.

###### (6) Formalization of differential-diagnostically relevant psychological disorders, depressive disorders and anxiety disorders
We try to clearly distinguish COVID-19 Fatigue from similar psychological disorders like depression and PTSD with the help of an ontological knowledge representation. We review diagnostic criteria and operationalization of these disorders, and explore their representation in existing disorder ontologies. By means of mathematical approaches a quantification of similarities, overlaps and differences between existing and established disorders should be reached.
