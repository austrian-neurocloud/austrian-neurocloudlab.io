---
Title: Funding by the Bundesministerium für Bildung, Wissenschaft und Forschung (BMBWF)
date: 2023-04-19
order: 1
website: https://www.bmbwf.gv.at/
Image: logo_bmbwf.svg
related_project: Austrian NeuroCloud (ANC, 2020-24)
---

## <u>Austrian NeuroCloud</u>
#### Funding period: 01.03.2020 - 31.12.2024
#### Funding sum: EURO 1.230.000,-

As a project, the Austrian NeuroCloud self-evidently constitutes the core foundation of the Austrian NeuroCloud as an open respository for neurocognitive research data. It funded by the Federal Ministry for Education, Science and Research Austria (BMBWF) as part of a large initiative to advance [Digital and Social Transformation in Higher Education](https://www.bmbwf.gv.at/Themen/HS-Uni/Aktuelles/Ausschreibung--Digitale-und-soziale-Transformation-in-der-Hochschulbildung-.html). It is further embedded into the vision of the [European Open Science Cloud (EOSC)](https://research-and-innovation.ec.europa.eu/strategy/strategy-2020-2024/our-digital-future/open-science/european-open-science-cloud-eosc_en)

###### (1) Data Management
Neuroimaging experiments generate vast amounts of heterogeneous data, including participant details, experiment designs, and large imaging files from various devices like EEG, MEG, and MRI. To standardize data formatting, the Brain Image Data Structure (BIDS) was developed, but its complexity can complicate its application across all data handling stages. While BIDS addresses data formatting issues, it doesn't provide a systematic data management solution. Often, data is scattered across different storage systems without version control or documented provenance. A first goal of the ANC is to create a comprehensive platform for modern data management in neuroscience that is easy to implement, adheres to FAIR principles, and follows best practices in data and software engineering.

###### (2) Data Classification
Austrian neurocognitive research has traditionally been strongly influenced by cognitive science and thus needs a stringent cognitive classification system to optimize sharing and (re-)use of neurocognitive datasets. Such a classification system is not provided by existing prototypes, but will be implemented in the Austrian NeuroCloud.

###### (3) Licensing
The provision of research data via the Austrian NeuroCloud requires a decision on several legal aspects. In this work package, we address issues of appropriate licensing of research data, but also general rules on how ANC can be used and what rules of conduct apply. Our goal is to enable users to make an informed decision about which license is most appropriate for their research data, as well as to create understandable terms of service and usage guidelines.

###### (4) Data Analysis

The data obtained in the course of an fMRI experiment often undergoes two complex procedures: image preprocessing and statistical analysis. Implementations of these procedures are often complex pipelines executing sophisticated software tools. The pipelines usually differ among researchers.
On the one hand, it is difficult to learn the variety of software tools and their parameters. Thus, the procedures cannot be easily standardized. This leads to tailored pipelines with hard-coded parameter values. On the other hand, rapid software evolution makes it hard to reproduce correct and working execution environments. The software is usually difficult to install and has complex dependencies which are difficult to maintain.
It is a crucial goal of ours to apply the recent advancements in software engineering to deliver modern data processing workflows and reproducible computational environments. The details of software execution will be documented to increase transparency and simplify reproducibility.

###### (5) Pilot studies
This work package aims at applying all the workflows and tools developed in the work packages data management, data classification, privacy and ethics and data analysis to two state-of-the art MRI datasets. The testing and integration of both data-sets into the pilot system helps to identify workflow problems. This allows valuable insights into the strengths and necessary improvements in the ANC’s capacity of storing, organizing and processing MRI study data.

###### (6) Research Data Lifecycle
This work package aims at monitoring the implementation of the FAIR Principles along the whole Research Data Lifecycle (RDL) by providing guidance on data management and classification, as well as by establishing standards on Best Scientific Practices, including domain-specific Data Management Plans and Tools to enable a robust research ecosystem for sustainable Data Governance.
This, however, is restricted to those components of the RDL that are in line with our mission as a repository provider, and thus the needs of our target group.


