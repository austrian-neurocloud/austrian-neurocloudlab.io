import json

AUTHOR = 'Mateusz Pawlik'
SITENAME = 'Austrian NeuroCloud'
SITEURL = ''

PATH = 'content'

STATIC_PATHS = ["metadata.json"]

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Limit rendered content
TAG_SAVE_AS = ''
TAGS_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
ARCHIVES_SAVE_AS = ''

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.toc': {}
    }
}

SITEMAP = {
    "format": "xml"
}

# Load metadata.json content
METADATA_FILE = "content/metadata.json"

with open(METADATA_FILE, "r", encoding="utf-8") as f:
    SITE_METADATA = json.load(f)

# Add the metadata to Pelican's global context
JINJA_GLOBALS = {
    "site_metadata": SITE_METADATA
}
